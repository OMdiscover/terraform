# Descriptif du projet

Ce projet a pour but de mettre en place une infrastructure composée :
- d'un réseau
- d'un sous-réseau
- d'un routeur
- d'une paire de clés généréé automatiquement pour ce projet, copiée dans le répertoire projet "projet_terraform/.ssh/" 
- d'une paire de clés généréé automatiquement pour les instances d'orchestration et copiées sur l'instance bastion dans le répertoire ".ssh"
- d'une instance bastion
- d'une instance master
- de 3 instances d'orchestration
- d'un server zabbix
- d'une IP flottante admin
- d'une IP flottante application
- de 3 groupes de sécurité
- d'un fichier "private_ips.txt" contenant les IP locales des instances, généré automatiquement et copié sur les instances
- d'un fichier "public_ips.txt" contenant les IP publiques des instances "bastion" et "master", généré automatiquement
- d'un fichier "hosts.ini" et "ssh.cfg" servant à lancer des commandes pour un projet Ansible
- d'un cluster kubernetes initialisé

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/projet_industriel_groupe1/terraform.git
```

# Installation de Terraform

- Installer Terraform :

```sh
TF_VERSION=1.3.6

echo "Downloading terraform binary ..."
if [ ! -e terraform_${TF_VERSION}_linux_amd64.zip ]; then  
  curl -O https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip
fi
if [ ! -x ~/bin/terraform ]; then 
  unzip terraform_${TF_VERSION}_linux_amd64.zip -d ~/bin/
fi
rm -f terraform_${TF_VERSION}_linux_amd64.zip
```

- Vérifier le bon fonctionnement de Terraform :

```sh
terraform -v 
```

> Terraform v1.3.6
> on linux_amd64

# Configuration de l'interaction avec Openstack

Pour pouvoir interagir avec Openstack, créer ou récupérer via horizon un fichier RC, et initialiser l'environnement :

```sh
cd terraform
source os-openrc.sh
env | grep OS_
```
> Le fichier "os-openrc.sh" correspond à votre fichier RC perso. N'oubliez pas de préciser le chemin.

# Renseigner les variables de Terraform

Il est nécessaire de paramétrer les variables suivantes afin de choisir vos caractéristiques de l'infrastructure (, nom du réseau, du sous-réseau, du routeur, le CIDR, ...). 
Pour cela, allez dans le fichier "main.tf" et veuillez à renseigner les variables. Vous pouvez modifier les variables à partir <ROUTER_NAME> jusqu'à <SECGROUP_INTERNAL_NETWORK_NAME>.

```sh
module "mod-os-private-network" {
  source                           = "./mod-os-private-network/"
  KEYPAIR_PATH                     = "$HOME/.ssh"
  EXTERNAL_NETWORK                 = "external"
  ROUTER_NAME                      = "router_tf_private"
  NETWORK_NAME                     = "network_tf_private"
  SUBNET_NAME                      = "subnet_tf_private"
  SUBNET_IP_RANGE                  = "192.168.1.0/24"
  DNS                              = ["192.44.75.10"]
  INSTANCE_BASTION_NAME            = "bastion"
  INSTANCE_BASTION_IMAGE           = "imta-docker"
  INSTANCE_BASTION_FLAVOR          = "s10.medium"
  INSTANCE_BASTION_KEY_PAIR        = "projet_terraform"
  MASTER_INSTANCE_NAME             = "master"
  MASTER_INSTANCE_IMAGE            = "imta-docker"
  MASTER_INSTANCE_FLAVOR           = "s10.medium"
  MASTER_INSTANCE_KEY_PAIR         = "cluster_key"
  INSTANCE_ORCHEST_NAME            = ["node01", "node02", "node03"]
  INSTANCE_ORCHEST_IMAGE           = "imta-docker"
  INSTANCE_ORCHEST_FLAVOR          = "s10.medium"
  INSTANCE_ORCHEST_KEY_PAIR        = "cluster_key"
  INSTANCE_ZABBIX_SERVER_NAME      = "zabbix-server"
  INSTANCE_ZABBIX_SERVER_IMAGE     = "imta-docker"
  INSTANCE_ZABBIX_SERVER_FLAVOR    = "s10.medium"
  INSTANCE_ZABBIX_SERVER_KEY_PAIR  = "projet_terraform"
  SECGROUP_BASTION_NAME            = "secgroup_bastion"
  SECGROUP_APPLICATION_NAME        = "secgroup_application"
  SECGROUP_INTERNAL_NETWORK_NAME   = "secgroup_internal_network"
  K3S_TOKEN                        = "cluster"
}
```

# Déploiement de l'architecture

```sh
cd terraform
terraform init
terraform plan
terraform apply
```
> Terraform apply vous demandera d'entrer une valeur (Enter a value:), taper "yes" et valider avec "entrée".

# Visualisation de l'architecture

Vous pouvez vérifier l'état des ressources vues par Terraform. Elles seront listées avec leurs attribus.

```sh
terraform show
```
Vous pouvez également lister les ressources par type dans Openstack, directement en ligne de commande :

```sh
openstack network list 
openstack subnet  list
openstack router list 
openstack floating ip list
openstack security group list 
```

L'architecture est graphiquement visualisable via Openstack :

```sh
Projet / Réseau / Topologie du réseau
```

# Visualiser le cluster Kubernetes

```sh
ssh -i .ssh/projet_terraform.pem ubuntu@<BASTION_PUBLIC_IP>
ssh -i .ssh/cluster_key.pem ubuntu@<MASTER_IP>
sudo kubectl get nodes
```

# Suppression de l'architecture

Si vous voulez supprimer l'architecture, lancer la commande suivante :

```sh
terraform destroy
```
