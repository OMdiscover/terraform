# Master instance
resource "openstack_compute_instance_v2" "master_instance" {
  name            = var.MASTER_INSTANCE_NAME 
  image_name      = var.MASTER_INSTANCE_IMAGE 
  flavor_name     = var.MASTER_INSTANCE_FLAVOR
  key_pair        = var.MASTER_INSTANCE_KEY_PAIR
  security_groups = ["${openstack_networking_secgroup_v2.secgroup_application.name}", "${openstack_networking_secgroup_v2.secgroup_internal_network.name}"]

  # Connexion au réseau interne
  network {
    name = "${openstack_networking_network_v2.internal_net.name}"
  }

  # Copie des IP locales dans un fichier .txt sur la machine hébergeante
  # Inventaire Ansible
  provisioner "local-exec" {
    command = "echo '${self.name} : ${self.access_ip_v4}' >> ip_files/private_ips.txt; echo '[master]\n${self.name} ansible_host=${self.access_ip_v4} PUB_IP=${openstack_networking_floatingip_v2.floatip_master.address}\n[node]' >> hosts.ini"  
  }

  depends_on = [openstack_compute_instance_v2.bastion_instance]
}

# Floating ip pour master
resource "openstack_networking_floatingip_v2" "floatip_master" {
  pool        = "external"
  description = "Floating IP pour l'instance ${var.MASTER_INSTANCE_NAME}"

  # Copie IP publique dans un fichier .txt sur la machine hébergeante
  provisioner "local-exec" {
    command = "echo '${var.MASTER_INSTANCE_NAME} : ${self.address}' >> ip_files/public_ips.txt"
  
  }

  depends_on = [openstack_networking_floatingip_v2.floatip_admin]
}

# Rattachement de l'ip flottante à l'instance master
resource "openstack_compute_floatingip_associate_v2" "fip_master" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_master.address}"
  instance_id = "${openstack_compute_instance_v2.master_instance.id}"

  depends_on = [openstack_networking_floatingip_v2.floatip_master]
}