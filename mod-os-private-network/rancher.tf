resource "null_resource" "install_kubernetes_master" {

  connection {
    type                = "ssh"
    user                = "ubuntu"
    bastion_private_key = "${openstack_compute_keypair_v2.keypair_project.private_key}"
    bastion_host        = "${openstack_networking_floatingip_v2.floatip_admin.address}"
    private_key         = "${openstack_compute_keypair_v2.keypair_cluster.private_key}"
    host                = "${openstack_compute_instance_v2.master_instance.access_ip_v4}" 
    agent               = "false"
    }
  
#  for_each = toset(var.INSTANCE_ORCHEST_NAME)
  
  # Install Kubernetes on the master instance
  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "curl -fL https://get.k3s.io | K3S_TOKEN=${var.K3S_TOKEN} sh -s - server --cluster-init",
      "mkdir ~/.kube",
      "sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube",
      ]
  }

  depends_on = [openstack_compute_instance_v2.bastion_instance,
  openstack_compute_instance_v2.orchestration_instance,
  openstack_compute_instance_v2.master_instance,
  null_resource.copy_keypair_cluster]
}



# Join the worker nodes to the Kubernetes cluster
resource "null_resource" "join_worker_nodes" {
  for_each = toset(var.INSTANCE_ORCHEST_NAME)

  # Connexion aux instances du cluster
  connection { 
    type                = "ssh"
    user                = "ubuntu"
    bastion_private_key = "${openstack_compute_keypair_v2.keypair_project.private_key}"
    bastion_host        = "${openstack_networking_floatingip_v2.floatip_admin.address}"
    private_key         = "${openstack_compute_keypair_v2.keypair_cluster.private_key}"
    host                = "${openstack_compute_instance_v2.orchestration_instance[each.key].access_ip_v4}" 
    agent               = "false"
    }

  provisioner "remote-exec" {
    inline = [
      "curl -fL https://get.k3s.io | K3S_TOKEN=${var.K3S_TOKEN} K3S_URL=https://${openstack_compute_instance_v2.master_instance.access_ip_v4}:6443 sh -s -",
      ]
  }

  depends_on = [openstack_compute_instance_v2.bastion_instance, 
  openstack_compute_instance_v2.orchestration_instance,
  null_resource.install_kubernetes_master]
}