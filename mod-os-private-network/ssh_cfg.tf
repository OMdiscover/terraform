resource "null_resource" "ssh_cfg" {
  provisioner "local-exec" { 
    command = "echo 'Host bastion\n Hostname ${openstack_networking_floatingip_v2.floatip_admin.address}\n User ubuntu\n IdentityFile .ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pem\nHost 192.*\n ProxyCommand ssh -F ssh.cfg -W %h:%p bastion\n User ubuntu\n IdentityFile .ssh/${var.INSTANCE_ORCHEST_KEY_PAIR}.pem\nHost *\n ControlMaster   auto\n ControlPath     .ssh/mux-%r@%h:%p\n ControlPersist  15m' > ssh.cfg"
  }
  
  depends_on = [openstack_compute_instance_v2.orchestration_instance]
}
