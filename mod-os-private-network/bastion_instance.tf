# Intance bastion
resource "openstack_compute_instance_v2" "bastion_instance" {
  name            = var.INSTANCE_BASTION_NAME 
  image_name      = var.INSTANCE_BASTION_IMAGE 
  flavor_name     = var.INSTANCE_BASTION_FLAVOR
  key_pair        = var.INSTANCE_BASTION_KEY_PAIR
  security_groups = ["${openstack_networking_secgroup_v2.secgroup_application.name}", "${openstack_networking_secgroup_v2.secgroup_bastion.name}", "${openstack_networking_secgroup_v2.secgroup_internal_network.name}"]

  # Connexion au réseau interne
  network {
    name = "${openstack_networking_network_v2.internal_net.name}"
  }

  # Copie des IP locales dans un fichier .txt sur la machine hébergeante
  # Inventaire Ansible
  provisioner "local-exec" {
    command = "echo '${self.name} : ${self.access_ip_v4}' > ip_files/private_ips.txt; echo '[admin]' > hosts.ini; echo ${self.name} ansible_host=${openstack_networking_floatingip_v2.floatip_admin.address} ansible_user=ubuntu ansible_ssh_private_key_file='.ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pem' >> hosts.ini"  
  }

  depends_on = [openstack_compute_keypair_v2.keypair_project,
    openstack_networking_floatingip_v2.floatip_admin,
    openstack_networking_subnet_v2.internal_subnet]
}

# Floating ip pour bastion
resource "openstack_networking_floatingip_v2" "floatip_admin" {
  pool        = "external"
  description = "Floating IP pour l'instance ${var.INSTANCE_BASTION_NAME}"

  # Copie IP publique dans un fichier .txt sur la machine hébergeante
  provisioner "local-exec" {
    command = "mkdir ip_files; echo '${var.INSTANCE_BASTION_NAME} : ${self.address}' > ip_files/public_ips.txt"
  
  }
}

# Rattachement de l'ip flottante à l'intance bastion
resource "openstack_compute_floatingip_associate_v2" "fip_admin" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_admin.address}"
  instance_id = "${openstack_compute_instance_v2.bastion_instance.id}"

  depends_on = [openstack_networking_floatingip_v2.floatip_admin]
}

# Copie sur bastion
resource "null_resource" "copy_local_ip_bastion" {

# Connexion à l'instance bastion
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${openstack_compute_keypair_v2.keypair_project.private_key}"
    host        = "${openstack_networking_floatingip_v2.floatip_admin.address}"
    agent       = "false"
  }

  # Copie de private_ips.txt sur l'instance bastion
  provisioner "file" {
    source      = "ip_files/private_ips.txt"
    destination = "private_ips.txt"
  }      

  depends_on = [openstack_compute_instance_v2.bastion_instance, 
    openstack_compute_instance_v2.orchestration_instance]
}


