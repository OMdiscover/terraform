# Intance pour l'orchestrateur
resource "openstack_compute_instance_v2" "orchestration_instance" {
  for_each        = toset (var.INSTANCE_ORCHEST_NAME) 
  name            = each.key
  image_name      = var.INSTANCE_ORCHEST_IMAGE
  flavor_name     = var.INSTANCE_ORCHEST_FLAVOR
  key_pair        = var.INSTANCE_ORCHEST_KEY_PAIR
  security_groups = ["${openstack_networking_secgroup_v2.secgroup_application.name}", "${openstack_networking_secgroup_v2.secgroup_internal_network.name}"]

  # Connexion au réseau interne
  network {
    name = "${openstack_networking_network_v2.internal_net.name}"
  }
  
  # Copie des IP locales dans un fichier .txt sur la machine hébergeante
  # Inventaire Ansible
  provisioner "local-exec" {
    command = "echo '${self.name} : ${self.access_ip_v4}' >> ip_files/private_ips.txt; echo ${self.name} ansible_host=${self.access_ip_v4} >> hosts.ini"  
  }

  depends_on = [openstack_compute_instance_v2.bastion_instance,
    openstack_compute_instance_v2.master_instance,
    openstack_compute_keypair_v2.keypair_cluster]
}


# Copie de private_ips.txt sur cluster
resource "null_resource" "copy_local_ip_cluster" {

  for_each = toset(var.INSTANCE_ORCHEST_NAME) 

# Connexion aux instances du cluster
  connection {
    type                = "ssh"
    user                = "ubuntu"
    bastion_private_key = "${openstack_compute_keypair_v2.keypair_project.private_key}"
    bastion_host        = "${openstack_networking_floatingip_v2.floatip_admin.address}"
    private_key         = "${openstack_compute_keypair_v2.keypair_cluster.private_key}"
    host                = "${openstack_compute_instance_v2.orchestration_instance[each.key].access_ip_v4}" 
    agent               = "false"
  }

  # Copie de private_ips.txt sur les instances du cluster
  provisioner "file" {
    source      = "ip_files/private_ips.txt"
    destination = "private_ips.txt"
  }      

  depends_on = [openstack_compute_instance_v2.bastion_instance, 
    openstack_compute_instance_v2.orchestration_instance]
}