variable "NETWORK_NAME" {
    type = string
    default = "internal_network"
}

variable "SUBNET_NAME" {
    type = string
    default = "internal_subnet"
}

variable "SUBNET_IP_RANGE" {
    type = string
    default = "192.168.61.0/24"
}

variable "DNS" {
    type = list(string)
    default = ["192.44.75.10"]
}

variable "EXTERNAL_NETWORK" {
  type    = string
  default = "external"
}

variable "ROUTER_NAME" {
  type    = string
  default = "internal_router"
}

variable "INSTANCE_BASTION_NAME" {
  type    = string
  default = "bastion"
}

variable "MASTER_INSTANCE_NAME" {
  type    = string
  default = "master"
}

variable "INSTANCE_ORCHEST_NAME" {
  type    = list(string)
  default = ["node01", "node02", "node03"]
}

variable "INSTANCE_ZABBIX_SERVER_NAME" {
  type    = string
  default = "zabbix-server"
}

variable "INSTANCE_BASTION_IMAGE" {
  type    = string
  default = "imta-docker"
}

variable "MASTER_INSTANCE_IMAGE" {
  type    = string
  default = "imta-docker"
}

variable "INSTANCE_ORCHEST_IMAGE" {
  type    = string
  default = "imta-docker"
}

variable "INSTANCE_ZABBIX_SERVER_IMAGE" {
  type    = string
  default = "imta-docker"
}

variable "INSTANCE_BASTION_FLAVOR" {
  type    = string
  default = "s10.medium"
}

variable "MASTER_INSTANCE_FLAVOR" {
  type    = string
  default = "s10.medium"
}

variable "INSTANCE_ORCHEST_FLAVOR" {
  type    = string
  default = "s10.medium"
}

variable "INSTANCE_ZABBIX_SERVER_FLAVOR" {
  type    = string
  default = "s10.medium"
}

variable "INSTANCE_BASTION_KEY_PAIR" {
  type    = string
  default = "projet_terraform"
}

variable "MASTER_INSTANCE_KEY_PAIR" {
  type    = string
  default = "projet_terraform"
}

variable "INSTANCE_ORCHEST_KEY_PAIR" {
  type    = string
  default = "cluster_key"
}

variable "INSTANCE_ZABBIX_SERVER_KEY_PAIR" {
  type    = string
  default = "cluster_key"
}

variable "SECGROUP_BASTION_NAME" {
  type    = string
  default = "secgroup_bastion"
}

variable "SECGROUP_APPLICATION_NAME" {
  type    = string
  default = "secgroup_application"
}

variable "SECGROUP_INTERNAL_NETWORK_NAME" {
  type    = string
  default = "secgroup_internal_network"
}

variable "KEYPAIR_PATH" {
  type    = string
  default = "$HOME/.ssh"
}

variable "K3S_TOKEN" {
  type    = string
  default = "cluster"
}