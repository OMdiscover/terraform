# Security group pour bastion
resource "openstack_networking_secgroup_v2" "secgroup_bastion" {
  name        = var.SECGROUP_BASTION_NAME
  description = "Security group pour l'instance ${var.INSTANCE_BASTION_NAME}"
}

# Rule SSH
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_bastion.id}"
}

# Security group pour application
resource "openstack_networking_secgroup_v2" "secgroup_application" {
  name        = var.SECGROUP_APPLICATION_NAME
  description = "Security group pour les instances d'application"
}

# Rule HTTP entrant
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_http_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_application.id}"
}

# Rule HTTP sortant
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_http_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_application.id}"
}

# Rule 30000 entrée
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_30000_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 30000
  port_range_max    = 30000
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_application.id}"
}

# Rule 32000 entrée
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_32000_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 32000
  port_range_max    = 32000
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_application.id}"
}

# Security group pour réseau interne
resource "openstack_networking_secgroup_v2" "secgroup_internal_network" {
  name        = var.SECGROUP_INTERNAL_NETWORK_NAME
  description = "Security group pour le réseau interne ${var.SUBNET_IP_RANGE}"
}

# Rule TCP entrant
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_tcp_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = var.SUBNET_IP_RANGE
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_network.id}"
}

# Rule UDP entrant
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_udp_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = var.SUBNET_IP_RANGE
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_network.id}"
}

# Rule TCP sortant
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_tcp_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  remote_ip_prefix  = var.SUBNET_IP_RANGE
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_network.id}"
}

# Rule UDP sortant
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule_udp_egress" {
  direction         = "egress"
  ethertype         = "IPv4"
  protocol          = "udp"
  remote_ip_prefix  = var.SUBNET_IP_RANGE
  security_group_id = "${openstack_networking_secgroup_v2.secgroup_internal_network.id}"
}