# Zabbix server
resource "openstack_compute_instance_v2" "zabbix_server" {
  name            = var.INSTANCE_ZABBIX_SERVER_NAME 
  image_name      = var.INSTANCE_ZABBIX_SERVER_IMAGE 
  flavor_name     = var.INSTANCE_ZABBIX_SERVER_FLAVOR
  key_pair        = var.INSTANCE_ZABBIX_SERVER_KEY_PAIR
  security_groups = ["${openstack_networking_secgroup_v2.secgroup_application.name}", "${openstack_networking_secgroup_v2.secgroup_bastion.name}", "${openstack_networking_secgroup_v2.secgroup_internal_network.name}"]

  # Connexion au réseau interne
  network {
    name = "${openstack_networking_network_v2.internal_net.name}"
  }

  # Copie des IP locales dans un fichier .txt sur la machine hébergeante
  # Inventaire Ansible
  provisioner "local-exec" {
    command = "echo '${self.name} : ${self.access_ip_v4}' >> ip_files/private_ips.txt; echo '[monitoring]\n${self.name} ansible_host=${openstack_networking_floatingip_v2.floatip_zabbix.address} ansible_user=ubuntu ansible_ssh_private_key_file=.ssh/${var.INSTANCE_ZABBIX_SERVER_KEY_PAIR}.pem' >> hosts.ini"  
  }

  depends_on = [openstack_compute_instance_v2.bastion_instance,
  openstack_compute_instance_v2.orchestration_instance]
}

# Floating ip pour bastion
resource "openstack_networking_floatingip_v2" "floatip_zabbix" {
  pool        = "external"
  description = "Floating IP pour l'instance ${var.INSTANCE_ZABBIX_SERVER_NAME}"

  # Copie IP publique dans un fichier .txt sur la machine hébergeante
  provisioner "local-exec" {
    command = "echo '${var.INSTANCE_ZABBIX_SERVER_NAME} : ${self.address}' >> ip_files/public_ips.txt"
  
  }

  depends_on = [openstack_networking_floatingip_v2.floatip_admin]
}

# Rattachement de l'ip flottante à zabbix server
resource "openstack_compute_floatingip_associate_v2" "fip_zabbix" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_zabbix.address}"
  instance_id = "${openstack_compute_instance_v2.zabbix_server.id}"

  depends_on = [openstack_networking_floatingip_v2.floatip_zabbix]
}